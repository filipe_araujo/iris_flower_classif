# DCC001 – Programação de Computadores
## Trabalho Prático 1: Sistema Classificador de Flores Íris

------------------------------------------

### Compilação e execução do programa:

- `gcc -o lista_de_decisao lista_de_decisao.c -lm`
- `./lista_de_decisao`

Obs.: o comando `-lm` é necessário para linkar a biblioteca `math.h`

### Comandos Iniciais:

- `TREINAR`: Inicia o modo de treinamento
- `TESTAR`: Inicia o modo de teste
- `SAIR`: Encerra o programa

### Amostras:

- `comprimento_sépala largura_sépala comprimento_pétala largura_pétala gênero_flor`
- `-1 -1 -1 PARAR`: Comando esperado para sair do modo de treinamento ou teste

Para obter exemplos reais de amostras, acesse: <https://archive.ics.uci.edu/ml/datasets/Iris>