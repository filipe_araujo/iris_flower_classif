# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <string.h>

float calcula_med( float parametro[], int total ) {

    int i = 0;
    float somatorio = 0, med=0;

    for( i=0; i<total; i++ ) {
        somatorio= somatorio + parametro[i];
    }

    med = somatorio/(total);

    return med;
}

float calcula_dp( float parametro[], int total, float med ) {

    float dp=0,somatorio=0;
    int i=0;

    for( i=0; i<total; i++ ) {
        somatorio = somatorio + pow((parametro[i] - med),2);
    }
    dp = sqrt(somatorio/(total-1));

    return dp;
}

int main( void ) {

    float cs, ls, cp, lp;
    char comando[21], genero[21];
    char* classe_predita;
    float acerto=0, erro=0;
    float acc=0;

    // vetores pras amostras coletadas
    float cs_set[50];
    float ls_set[50];
    float cp_set[50];
    float lp_set[50];

    float cs_versi[50];
    float ls_versi[50];
    float cp_versi[50];
    float lp_versi[50];

    float cs_virg[50];
    float ls_virg[50];
    float cp_virg[50];
    float lp_virg[50];

    // Declaração dos contadores das 3 classes
    int cont_set=0, cont_versi=0, cont_virg=0;

    // média das amostras coletadas
    float med_set_cs;
    float med_set_ls;
    float med_set_cp;
    float med_set_lp;

    float med_versi_cs;
    float med_versi_ls;
    float med_versi_cp;
    float med_versi_lp;

    float med_virg_cs;
    float med_virg_ls;
    float med_virg_cp;
    float med_virg_lp;

    // dp das amostras coletadas
    float dp_set_cs;
    float dp_set_ls;
    float dp_set_cp;
    float dp_set_lp;

    float dp_versi_cs;
    float dp_versi_ls;
    float dp_versi_cp;
    float dp_versi_lp;

    float dp_virg_cs;
    float dp_virg_ls;
    float dp_virg_cp;
    float dp_virg_lp;

    //colocar na area de declaracao de variaveis
    int executar_programa, ler_amostras;

    //antes de executar o while que contem os ifs que testam qual comando e'
    executar_programa = 1;

    while( executar_programa ) {
        printf( "Entre com o comando: " );
        scanf( "%s", comando ); //ler o comando

        if( strcmp(comando, "SAIR") == 0 ) {
            printf( "ENCERRANDO O PROGRAMA!\n" );
            executar_programa = 0;
        }else if( strcmp(comando, "TREINAR") == 0 ) { //segue uma serie de else ifs para testar se o comando e' invalido ou e' um outro comando valido
            ler_amostras = 1;
            while( ler_amostras ) {
                printf( "TREINAMENTO INICIADO, ENTRE COM AMOSTRAS: " );
                scanf( "%f %f %f %f %s", &cs, &ls, &cp, &lp, genero );

                if( cs == -1.0 && ls == -1.0 && cp == -1.0 && lp == -1.0 && strcmp(genero, "PARAR") == 0 ) {
                    ler_amostras = 0;
                }else if( cs <= 0 || ls <= 0 || cp <=0 || lp <=0 || strcmp(genero, "Iris-setosa") !=0 && strcmp(genero, "Iris-versicolor") !=0 && strcmp(genero, "Iris-virginica") !=0 ) {
                    printf( "AMOSTRA INVALIDA!\n" );
                }else if( strcmp(genero, "Iris-setosa") == 0 ) {
                    cs_set[cont_set] = cs;
                    ls_set[cont_set] = ls;
                    cp_set[cont_set] = cp;
                    lp_set[cont_set] = lp;
                    cont_set++;
                }else if( strcmp(genero, "Iris-versicolor") == 0 ) {
                    cs_versi[cont_versi] = cs;
                    ls_versi[cont_versi] = ls;
                    cp_versi[cont_versi] = cp;
                    lp_versi[cont_versi] = lp;
                    cont_versi++;
                }else if( strcmp(genero, "Iris-virginica") == 0 ) {
                    cs_virg[cont_virg] = cs;
                    ls_virg[cont_virg] = ls;
                    cp_virg[cont_virg] = cp;
                    lp_virg[cont_virg] = lp;
                    cont_virg++;
                }
            }

            // calculo das meds e dps-padrão
            printf( "PARAMETROS DO MODELO: \n" );
            printf( "Iris-setosa\n" );

            med_set_cs = calcula_med(cs_set,cont_set);
            dp_set_cs = calcula_dp(cs_set,cont_set,med_set_cs);

            med_set_ls = calcula_med(ls_set,cont_set);
            dp_set_ls = calcula_dp(ls_set,cont_set,med_set_ls);

            med_set_cp = calcula_med(cp_set,cont_set);
            dp_set_cp = calcula_dp(cp_set,cont_set,med_set_cp);

            med_set_lp = calcula_med(lp_set,cont_set);
            dp_set_lp = calcula_dp(lp_set,cont_set,med_set_lp);

            printf("CS: med = %.2f, DP = %.2f\n", med_set_cs, dp_set_cs);
            printf("LS: med = %.2f, DP = %.2f\n", med_set_ls, dp_set_ls);
            printf("CP: med = %.2f, DP = %.2f\n", med_set_cp, dp_set_cp);
            printf("LP: med = %.2f, DP = %.2f\n", med_set_lp, dp_set_lp);

            printf( "Iris-versicolor\n" );

            med_versi_cs = calcula_med(cs_versi,cont_versi);
            dp_versi_cs = calcula_dp(cs_versi,cont_versi,med_versi_cs);

            med_versi_ls = calcula_med(ls_versi,cont_versi);
            dp_versi_ls = calcula_dp(ls_versi,cont_versi,med_versi_ls);

            med_versi_cp = calcula_med(cp_versi,cont_versi);
            dp_versi_cp = calcula_dp(cp_versi,cont_versi,med_versi_cp);

            med_versi_lp = calcula_med(lp_versi,cont_versi);
            dp_versi_lp = calcula_dp(lp_versi,cont_versi,med_versi_lp);

            printf("CS: med = %.2f, DP = %.2f\n", med_versi_cs, dp_versi_cs);
            printf("LS: med = %.2f, DP = %.2f\n", med_versi_ls, dp_versi_ls);
            printf("CP: med = %.2f, DP = %.2f\n", med_versi_cp, dp_versi_cp);
            printf("LP: med = %.2f, DP = %.2f\n", med_versi_lp, dp_versi_lp);

            printf( "Iris-virginica\n" );

            med_virg_cs = calcula_med(cs_virg,cont_virg);
            dp_virg_cs = calcula_dp(cs_virg,cont_virg,med_virg_cs);

            med_virg_ls = calcula_med(ls_virg,cont_virg);
            dp_virg_ls = calcula_dp(ls_virg,cont_virg,med_virg_ls);

            med_virg_cp = calcula_med(cp_virg,cont_virg);
            dp_virg_cp = calcula_dp(cp_virg,cont_virg,med_virg_cp);

            med_virg_lp = calcula_med(lp_virg,cont_virg);
            dp_virg_lp = calcula_dp(lp_virg,cont_virg,med_virg_lp);

            printf("CS: med = %.2f, DP = %.2f\n", med_virg_cs, dp_virg_cs);
            printf("LS: med = %.2f, DP = %.2f\n", med_virg_ls, dp_virg_ls);
            printf("CP: med = %.2f, DP = %.2f\n", med_virg_cp, dp_virg_cp);
            printf("LP: med = %.2f, DP = %.2f\n", med_virg_lp, dp_virg_lp);
        }
        else if( strcmp(comando,"TESTE") == 0 ) {
            ler_amostras = 1;
            while( ler_amostras ) {
                printf( "TESTE INICIADO, ENTRE COM AMOSTRAS: " );
                scanf( "%f %f %f %f %s", &cs, &ls, &cp, &lp, genero );
                if( cs == -1.0 && ls == -1.0 && cp == -1.0 && lp == -1.0 && strcmp(genero, "PARAR") == 0 ) {
                    ler_amostras = 0;
                    break;
                }else if( cs <= 0 || ls <= 0 || cp <=0 || lp <=0 || strcmp(genero, "Iris-setosa") !=0 && strcmp(genero, "Iris-versicolor") !=0 && strcmp(genero, "Iris-virginica") !=0 )
                    printf( "AMOSTRA INVALIDA!\n" );
                else if( fabs(cs - med_set_cs) < 2* dp_set_cs && fabs(ls - med_set_ls) < 2* dp_set_ls && fabs(cp - med_set_cp) < 2* dp_set_cp && fabs(lp - med_set_lp) < 2* dp_set_lp ) {
                    classe_predita = "Iris-setosa";
                    printf( "CLASSE PREDITA: %s\n", classe_predita );
                    printf( "CLASSE REAL: %s\n", genero );
                }else if( fabs(cs - med_versi_cs) < 2* dp_versi_cs && fabs(ls - med_versi_ls) < 2* dp_versi_ls && fabs(cp - med_versi_cp) < 2* dp_versi_cp && fabs(lp - med_versi_lp) < 2* dp_versi_lp ) {
                    classe_predita = "Iris-versicolor";
                    printf( "CLASSE PREDITA: %s\n", classe_predita );
                    printf( "CLASSE REAL: %s\n", genero );
                }else if( fabs(cs - med_virg_cs) < 2* dp_virg_cs && fabs(ls - med_virg_ls) < 2* dp_virg_ls && fabs(cp - med_virg_cp) < 2* dp_virg_cp && fabs(lp - med_virg_lp) < 2* dp_virg_lp ) {
                    classe_predita = "Iris-virginica";
                    printf( "CLASSE PREDITA: %s\n", classe_predita );
                    printf( "CLASSE REAL: %s\n", genero );
                }else {
                    classe_predita = "Desconhecida";
                    printf( "CLASSE PREDITA: %s\n", classe_predita );
                    printf( "CLASSE REAL: %s\n", genero );
                }

                if( strcmp(genero, classe_predita) == 0 )
                    acerto++;//contabilizar acerto
                else
                    erro++;//contabilizar erro
            }

            //printf( "\nAcertos: %.1f  Erros: %.1f\n", acerto, erro );
            printf( "\nACURACIA DO MODELO: %.2f%%\n", acc = (acerto/(acerto+erro))*100 );
        }
        else
            printf( "COMANDO INVALIDO!\n" );
    }

    return 0;
}